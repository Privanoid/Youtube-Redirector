Redirecting Youtube to Invidious and new Hooktube
-----

On 16th july [Hooktube](https://hooktube.com)'s changelog was updated with the following entry:

> July 16: YouTube api features are back but mp4 `<video>` is replaced with the standard YT video embed. HookTube is now effectively just a light-weight version of youtube and useless to the 90% of you primarily concerned with denying Google data and seeing videos blocked by your governments.

Obvoiusly now we can't trust Hooktube anymore, so we need an alternative.

For now the best option I've found is [Invidious](https://invidio.us/) (Github's repository [here](https://github.com/omarroth/invidious)). **If you know any other more privacy friendly alternative please let me know**. It has [many more features][invidiousredditlink] than HookTube like logging into your YouTube account. From a privacy point of view, I don't recommend using them, but that's up to you. I. e. the sound only playback needs *Fontawesome* to work, and this could be a [privacy concern][fontawesomeprivacy] be cause of its use of the CDN's. Even so the main and more interesting features (like searching channels and videos) work without these. Here you can see the minimum necessary scripts for using it (`ytimg.com` isn't really necessary either):  

uMatrix | NoScript
:---: | :---:
![uMatrix minimum][uMatrixjpg]|![NoScript minimum][noscriptjpg]

So, we need now add-ons to redirect our searches to Invidious. I recommend [Redirector](https://github.com/einaregilsson/Redirector) or [Invidious Redirect](https://addons.mozilla.org/firefox/addon/hooktube-redirect) (previously Hooktube Redirect).

[Redirector](https://github.com/einaregilsson/Redirector) redirects webs, frames, etc. according to rules created by the user before the request is sent to the original web. You can create them yourself (it's useful and easy to learn) or import a configuration file. I've created and uploaded one for Invidious.

### Redirector profiles for Invidious

You can download the *invidious_config.txt* file or copy the text below in a .txt and import it to Redirector.

```
{
    "createdBy": "Redirector v3.1.1",
    "createdAt": "",
    "redirects": [
        {
            "description": "YouTube to Invidio.us",
            "exampleUrl": "https://www.youtube.com/channel/UCaKZA66vM_TUpetUNohmR0A",
            "exampleResult": "https://invidio.us/channel/UCaKZA66vM_TUpetUNohmR0A",
            "error": null,
            "includePattern": "http*://*.youtube.com/*",
            "excludePattern": "",
            "redirectUrl": "https://invidio.us/$3",
            "patternType": "W",
            "processMatches": "noProcessing",
            "disabled": false,
            "appliesTo": [
                "main_frame",
                "sub_frame",
                "image",
                "object",
                "xmlhttprequest",
                "other"
            ]
        },
        {
            "description": "youtu.be to Invidious",
            "exampleUrl": "https://youtu.be/",
            "exampleResult": "https://invidio.us/",
            "error": null,
            "includePattern": "http*://youtu.be/",
            "excludePattern": "",
            "redirectUrl": "https://invidio.us/",
            "patternType": "W",
            "processMatches": "noProcessing",
            "disabled": false,
            "appliesTo": [
                "main_frame",
                "sub_frame",
                "image",
                "object",
                "xmlhttprequest"
            ]
        },
        {
            "description": "youtu.be videos to Invidious",
            "exampleUrl": "https://youtu.be/IXdNnw99-Ic",
            "exampleResult": "https://invidio.us/watch?v=IXdNnw99-Ic",
            "error": null,
            "includePattern": "http*://youtu.be/*",
            "excludePattern": "http*://youtu.be/",
            "redirectUrl": "https://invidio.us/watch?v=$2",
            "patternType": "W",
            "processMatches": "noProcessing",
            "disabled": false,
            "appliesTo": [
                "main_frame",
                "sub_frame",
                "image",
                "object",
                "xmlhttprequest"
            ]
        }
    ]
}

```

[invidiousredditlink]: https://www.reddit.com/r/SideProject/comments/8wvazc/invidous_alternative_frontend_to_youtube/ "link to developer's Reddit post"
[fontawesomeprivacy]: https://fontawesome.com/privacy "Fontawesome Privacy Policy"

[uMatrixjpg]: https://gitlab.com/Privanoid/Youtube-Redirector/raw/master/images/invidious_umatrix.jpg
[noscriptjpg]: https://gitlab.com/Privanoid/Youtube-Redirector/raw/master/images/invidious_noscript.jpg
